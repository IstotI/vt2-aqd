Makes **A Quiet Drink** available in the mission menu.  
Also comes with a custom area video background but no music.

~~Note: Reloading mods while in the keep crashes the game.~~ Fixed  
Note: VMB currently does not properly copy .stream files so those have to be manually copied from the .temp folder into the bundleV2 folder