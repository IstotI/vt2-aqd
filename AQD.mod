return {
	run = function()
		fassert(rawget(_G, "new_mod"), "AQD must be lower than Vermintide Mod Framework in your launcher's load order.")

		new_mod("AQD", {
			mod_script       = "scripts/mods/AQD/AQD",
			mod_data         = "scripts/mods/AQD/AQD_data",
			mod_localization = "scripts/mods/AQD/AQD_localization",
		})

		-- We return an empty table to silence warnings about not returning a callback table.
		return { }
	end,
	packages = {
		"resource_packages/AQD/AQD",
	},
}
