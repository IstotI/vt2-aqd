local mod = get_mod("AQD")

return {
	name = mod:localize("mod_name"),
	description = mod:localize("mod_description"),
}
