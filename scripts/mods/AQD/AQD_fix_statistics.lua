local mod = get_mod("AQD") --Get the mod

--From scripts\managers\backend\statistics_definitions.lua

local player = StatisticsDefinitions.player
local level_key = "dlc_celebrate_crawl"

player.completed_levels[level_key] = {
	value = 0,
	source = "player_data",
	sync_on_hot_join = true,
	sync_to_host = true,
	database_name = "completed_levels_" .. level_key,
	name = level_key,
}

player.played_levels_quickplay[level_key] = {
	value = 0,
	source = "player_data",
	sync_to_host = true,
	database_name = "played_levels_quickplay_" .. level_key,
	name = level_key,
}

player.played_levels_weekly_event[level_key] = {
	value = 0,
	source = "player_data",
	sync_to_host = true,
	database_name = "played_levels_weekly_event_" .. level_key,
	name = level_key,
}

for _, profile in pairs(SPProfiles) do
	if profile.display_name ~= "empire_soldier_tutorial" then
		local database_name = "completed_levels_" .. profile.display_name .. "_" .. level_key
		player["completed_levels_" .. profile.display_name][level_key] = {
			value = 0,
			source = "player_data",
			database_name = database_name,
			name = level_key,
		}
	end
end

local level_difficulty_name = level_key .. "_difficulty_completed"
LevelDifficultyDBNames[level_key] = level_difficulty_name

player.completed_levels_difficulty[level_difficulty_name] = {
	value = 0,
	source = "player_data",
	sync_on_hot_join = true,
	database_name = level_difficulty_name,
	name = level_difficulty_name,
}

NetworkLookup.statistics_path_names[#NetworkLookup.statistics_path_names+1] = level_difficulty_name
NetworkLookup.statistics_path_names[level_difficulty_name] = #NetworkLookup.statistics_path_names
NetworkLookup.statistics_path_names[#NetworkLookup.statistics_path_names+1] = level_key
NetworkLookup.statistics_path_names[level_key] = #NetworkLookup.statistics_path_names

local grimoire_name = "collected_grimoire_" .. level_key
player.collected_grimoires[level_key] = {
	value = 0,
	source = "player_data",
	database_name = grimoire_name,
	name = level_key,
}

local tome_name = "collected_tome_" .. level_key
player.collected_tomes[level_key] = {
	value = 0,
	source = "player_data",
	database_name = tome_name,
	name = level_key,
}

local die_name = "collected_die_" .. level_key
player.collected_dice[level_key] = {
	value = 0,
	source = "player_data",
	database_name = die_name,
	name = level_key,
}

local painting_name = "collected_painting_scraps_" .. level_key
player.collected_painting_scraps[level_key] = {
	value = 0,
	source = "player_data",
	database_name = painting_name,
	name = level_key,
}

for career, _ in pairs(CareerSettings) do
	if career ~= "empire_soldier_tutorial" then
		player.completed_career_levels[career][level_key] = {}

		for diff, _ in pairs(DifficultySettings) do
			local database_name = "completed_career_levels_" .. career .. "_" .. level_key .. "_" .. diff
			player.completed_career_levels[career][level_key][diff] = {
				value = 0,
				source = "player_data",
				database_name = database_name,
				name = diff,
			}
		end
	end
end
