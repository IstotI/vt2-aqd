return {
	mod_name = {
		en = "A Quiet Drink",
	},
	mod_description = {
		en = "Makes A Quiet Drink available in the mission menu.\nAlso comes with a custom area video background but no music.",
	},
}
